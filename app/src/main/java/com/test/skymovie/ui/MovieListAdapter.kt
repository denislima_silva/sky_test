package com.test.skymovie.ui

import android.app.Activity
import android.app.ActivityOptions
import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.squareup.picasso.Picasso
import com.test.skymovie.models.Movies
import kotlinx.android.synthetic.main.movie_item.view.*
import android.os.Build




class MovieListAdapter(private val movies: List<Movies>,
                       private val context: Context
): RecyclerView.Adapter<MovieListAdapter.ViewHolder>() {
    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
        val movies = movies[p1]
        p0.let {
            it.bindView(movies)
        }

        p0?.img.setOnClickListener{

            val intent = Intent(context, ActivityDetalhes::class.java)
            intent.putExtra("overview", movies.overview)
            intent.putExtra("backdrops_url", movies.backdrops_url[0])
            intent.putExtra("title", movies.title)
            intent.putExtra("duration", movies.duration)
            intent.putExtra("release_year", movies.release_year)

            // TRANSITIONS
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {


                context.startActivity(intent,
                    ActivityOptions.makeSceneTransitionAnimation(context as Activity).toBundle())

            } else {

                context.startActivity(intent)
            }

            }


    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(com.test.skymovie.R.layout.movie_item, p0, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return movies.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val img = itemView.imgMovie
        val title = itemView.txtTitulo

        fun bindView(movies: Movies) {
            val title = itemView.txtTitulo
            val img = itemView.imgMovie

            title.text = movies.title
            Picasso.with(itemView.context).load(movies.cover_url).placeholder(com.test.skymovie.R.drawable.ic_action_charge_small).error(
                com.test.skymovie.R.drawable.ic_action_triste_small).into(img)

        }

    }
}