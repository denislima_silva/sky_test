package com.test.skymovie.ui

import android.os.Build
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.transition.Explode
import android.transition.Fade
import android.view.MenuItem
import com.squareup.picasso.Picasso
import com.test.skymovie.R
import kotlinx.android.synthetic.main.activity_detalhes.*

class ActivityDetalhes: AppCompatActivity() {
    var descricao: String = ""
    var img: String = ""
    var title: String = ""
    var duration: String = ""
    var release_year: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {

        //Condigurando transicao
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP ){
            val trans1 = Explode()
            trans1.duration = 3000
            val trans2 = Fade()
            trans2.duration = 3000

            window.enterTransition = trans1
            window.returnTransition = trans2
        }

        super.onCreate(savedInstanceState)
        setContentView(com.test.skymovie.R.layout.activity_detalhes)

        getSupportActionBar()?.setDisplayHomeAsUpEnabled(true)
        getSupportActionBar()?.setHomeButtonEnabled(true)

        descricao = intent.getSerializableExtra("overview").toString()
        img = intent.getSerializableExtra("backdrops_url").toString()
        title = intent.getSerializableExtra("title").toString()
        duration = intent.getSerializableExtra("duration").toString()
        release_year = intent.getSerializableExtra("release_year").toString()

        textView4.text = descricao
        textView2.text = title
        textView6.text = getString(R.string.duracao)+" "+duration
        textView5.text = getString(R.string.ano)+" "+release_year


        Picasso.with(applicationContext).load(img).placeholder(R.drawable.ic_action_charge_small).error(R.drawable.ic_action_triste_small).into(imageView)


    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home
            ->
                //startActivity(new Intent(this, ActivityDetalhe.class));
                finish()
            else -> {
            }
        }
        return true
    }
}