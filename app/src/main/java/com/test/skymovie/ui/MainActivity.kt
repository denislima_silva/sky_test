package com.test.skymovie.ui

import android.content.DialogInterface
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import com.test.skymovie.R
import com.test.skymovie.models.Movies
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*
import android.support.v7.widget.GridLayoutManager
import android.transition.Explode
import android.widget.ProgressBar
import com.test.skymovie.utils.RetrofitInitializer
import android.view.MenuItem
import android.widget.TextView
import com.test.skymovie.utils.VerificaConexao
import java.lang.Exception
import android.transition.Fade


class MainActivity : AppCompatActivity() {
    private var myAdapter: MovieListAdapter? = null
    private var myCompositeDisposable: CompositeDisposable? = null
    private var myRetroCryptoArrayList: ArrayList<Movies>? = null
    private var progressBar: ProgressBar? = null
    private var txtView: TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {

        //Condigurando transicao
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP ){
            val trans1 = Explode()
            trans1.duration = 3000
            val trans2 = Fade()
            trans2.duration = 3000

            window.exitTransition = trans1
            window.reenterTransition = trans2
        }

        super.onCreate(savedInstanceState)
        setContentView(com.test.skymovie.R.layout.activity_main)

        getSupportActionBar()?.setDisplayHomeAsUpEnabled(true)
        getSupportActionBar()?.setHomeButtonEnabled(true)

        progressBar = findViewById(com.test.skymovie.R.id.progressBar)
        txtView = findViewById(com.test.skymovie.R.id.textView)

        myCompositeDisposable = CompositeDisposable()

        val verificaConexao = VerificaConexao()
        if( verificaConexao.verificaConexao(applicationContext)) {

            initRecyclerView()

            try {
                loadData()
            }
            catch (e: Exception){
                val dialogBuilder = AlertDialog.Builder(this)
                dialogBuilder.setTitle(getString(com.test.skymovie.R.string.atencao))
                dialogBuilder.setMessage(getString(com.test.skymovie.R.string.msg_alert_2))
                dialogBuilder.setPositiveButton(getString(com.test.skymovie.R.string.ok_alert), DialogInterface.OnClickListener {
                        dialog, id -> finish()
                })
                dialogBuilder.show()
            }

        }
        else {

            val dialogBuilder = AlertDialog.Builder(this)
            dialogBuilder.setTitle(getString(com.test.skymovie.R.string.atencao))
            dialogBuilder.setMessage(getString(com.test.skymovie.R.string.mensagem_alert))
            dialogBuilder.setPositiveButton(getString(com.test.skymovie.R.string.ok_alert), DialogInterface.OnClickListener {
                    dialog, id -> finish()
            })
            dialogBuilder.show()

            //finish()

        }
    }

    private fun initRecyclerView() {

       /* val layoutManager : RecyclerView.LayoutManager = LinearLayoutManager(this)
        mainList.layoutManager = layoutManager */

        val layoutManager = GridLayoutManager(this, 2)
        mainList.setLayoutManager(layoutManager)

    }

    private fun loadData() {

        val requestInterface = RetrofitInitializer()
        progressBar!!.visibility = ProgressBar.VISIBLE


        myCompositeDisposable?.add(requestInterface.RetrofitInitializer().list()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(this::handleResponse))

    }

    private fun handleResponse(cryptoList: List<Movies>) {

        txtView!!.visibility = TextView.VISIBLE

        myRetroCryptoArrayList = ArrayList(cryptoList)
        myAdapter = MovieListAdapter(myRetroCryptoArrayList!!, this)
        mainList.adapter = myAdapter
        progressBar!!.visibility = ProgressBar.GONE

    }

    override fun onDestroy() {
        super.onDestroy()
        myCompositeDisposable?.clear()

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home
            ->
                //startActivity(new Intent(this, ActivityDetalhe.class));
                finish()
            else -> {
            }
        }
        return true
    }
}

