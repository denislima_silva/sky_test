package com.test.skymovie.utils

class Constants {
    companion object {
        const val BASE_URL = "https://sky-exercise.herokuapp.com/api/"
    }
}