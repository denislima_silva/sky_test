package com.test.skymovie.utils

import com.test.skymovie.api.ApiServiceInterface
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitInitializer {

    fun RetrofitInitializer(): ApiServiceInterface {

        val requestInterface = Retrofit.Builder()
            .baseUrl(Constants.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build().create(ApiServiceInterface::class.java)

        return requestInterface
    }

}