package com.test.skymovie.utils

import android.content.Context
import android.net.ConnectivityManager

class VerificaConexao {

    fun verificaConexao(cn: Context): Boolean {
        val conectado: Boolean
        val conectivtyManager = cn.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (conectivtyManager.activeNetworkInfo != null
            && conectivtyManager.activeNetworkInfo.isAvailable
            && conectivtyManager.activeNetworkInfo.isConnected
        ) {
            conectado = true
        } else {
            conectado = false
        }
        return conectado
    }

}