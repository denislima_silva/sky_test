package com.test.skymovie.api

import android.provider.ContactsContract
import com.test.skymovie.models.Movies
import retrofit2.http.GET
import retrofit2.http.Path
import java.util.*
import io.reactivex.Observable

interface ApiServiceInterface {

    @GET("Movies")
    fun list(): Observable<List<Movies>>

    /*
    @GET("issues/{number}")
    fun getDetalhes(@Path("number") number: String): Observable<Movies>
 */
}