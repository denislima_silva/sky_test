package com.test.skymovie.models

data class Movies (val id: String, val cover_url: String, val title: String, val overview: String, val backdrops_url: List<String>,
                   val duration: String,
                   val release_year: String)