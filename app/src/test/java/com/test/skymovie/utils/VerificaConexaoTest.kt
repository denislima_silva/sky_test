package com.test.skymovie.utils

import android.content.Context
import android.net.ConnectivityManager
import androidx.test.InstrumentationRegistry
import org.junit.Test

import org.junit.Assert.*
import org.junit.Before
import org.junit.runner.RunWith
import org.junit.runners.*
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config


@RunWith(RobolectricTestRunner::class)
@Config(manifest= Config.NONE)
class VerificaConexaoTest {
    private var instrumentationCtx: Context? = null

    @Before
     fun setUp() {
        instrumentationCtx = InstrumentationRegistry.getContext()

    }

    @Test
    fun a(){
        verificaConexao(instrumentationCtx!!)
    }

        @Parameterized.Parameters
        fun verificaConexao(instrumentationCtx: Context) {
            val conectado: Boolean? = null
            val conectivtyManager = this.instrumentationCtx?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            if (conectivtyManager.activeNetworkInfo != null
                && conectivtyManager.activeNetworkInfo.isAvailable
                && conectivtyManager.activeNetworkInfo.isConnected
            ) {
                if (conectado != null) {
                    assertTrue(conectado)
                }
            } else {
                if (conectado != null) {
                    assertFalse(conectado)
                }
            }

        }


}