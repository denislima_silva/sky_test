package com.test.skymovie.utils

import android.content.Context
import androidx.test.InstrumentationRegistry
import com.test.skymovie.api.ApiServiceInterface
import org.junit.Test

import org.junit.Assert.*
import org.junit.Before
import org.junit.runner.RunWith
import org.mockito.Mockito.verify
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

@RunWith(RobolectricTestRunner::class)
@Config(manifest= Config.NONE)
class RetrofitInitializerTest {
    private var instrumentationCtx: Context? = null

    @Before
    fun setUp() {
        instrumentationCtx = InstrumentationRegistry.getContext()

    }

        fun RetrofitInitializer(): ApiServiceInterface {

            val requestInterface = Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build().create(ApiServiceInterface::class.java)

            return requestInterface
        }

    @Test
    fun a(){
        RetrofitInitializer()
    }


}